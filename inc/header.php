<!DOCTYPE HTML>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>PDO</title>
  <style>
body {
  font-family: arial;
  font-size: 18px;
  line-height: 22px;
  margin: 0 auto;
  width: 800px;
}
.headeroption {
  background: green url("img/php.png") no-repeat scroll 56px 18px;
  height: 80px;
  overflow: hidden;
  padding-left: 160px;
}
.footeroption{height:80px;background: green; overflow:hidden;}
.headeroption h2 {
  color: white;
  font-size: 30px;
  padding-top: 5px;
  text-shadow: 0 1px 1px #fff;
  text-align: center;
}
.footeroption h2 {
  background: rgba(0, 0, 0, 0) url("img/logo.png") no-repeat scroll 65px 0;
  color: #000;
  font-size: 30px;
  padding-bottom: 13px;
  padding-top: 10px;
  text-align: center;
  text-shadow: 1px 1px 1px #fff;
}
.content {
  background: gray none repeat scroll 0 0;
  border: 6px solid green;
  font-size: 16px;
  line-height: 22px;
  margin-bottom: 8px;
  margin-top: 8px;
  min-height: 420px;
  overflow: hidden;
  padding: 10px;
}
  </style>
</head>
<body>
  <header class="headeroption">
    <h2>PHP Data Object(PDO)</h2>
  </header>
  
  <section class="content">